{ pkgs ? import <nixpkgs> {} }:
with pkgs;
rustPlatform.buildRustPackage rec {
  name = "tokio-webhook2muc-${version}";
  version = "0.0.1";

  src = ./.;
  buildInputs = with pkgs; [ pkgconfig openssl ];

  cargoSha256 = "1sna8mncjnkqh8hf9xshlkh4dqp29n3pz2cagglp3sw4m8fzzdp4";

  meta = with lib; {
    description = "Webhook to XMPP MUC bot";
    homepage = https://gitlab.com/xmpp-rs;
    license = licenses.gpl3Plus;
    maintainers = [ maintainers.astro ];
    platforms = platforms.all;
  };
}
